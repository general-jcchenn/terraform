resource "aws_iam_role" "lambda_role" {
    name                = "Hello_Lambda_Function_Role"
    assume_role_policy  = templatefile("${path.module}/iam-policy/trust-policies/iam-role-trust-policy.tpl", { none = "none" })
}

resource "aws_iam_policy" "iam_policy_for_lambda" {
    name            = "aws_iam_policy_for_terraform_aws_lambda_role"
    path            = "/"
    description     = "AWS IAM Policy for managing aws lambda role"
    policy          = templatefile("${path.module}/iam-policy/role-policies/iam-role-lambda-policy.tpl", { none = "none" })
}

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
    role        = aws_iam_role.lambda_role.name
    policy_arn  = aws_iam_policy.iam_policy_for_lambda.arn
}
