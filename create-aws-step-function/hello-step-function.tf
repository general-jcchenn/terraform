resource "aws_sfn_state_machine" "hello-workflow" {
  name     = "hello-workflow"
  role_arn = aws_iam_role.lambda_role.arn
  definition = templatefile("${path.module}/states/hello-states-flow.json/", { 
      hello_function_arn            = "${aws_lambda_function.hello_lambda_func.arn}",
      hello_function_exception_arn  = "${aws_lambda_function.hello_exception_lambda_func.arn}"
    })
  depends_on = [aws_lambda_function.hello_lambda_func]  
}

