# define lambda function
data "archive_file" "hello_lambda_code" {
    type        = "zip"
    source_dir  = "${path.module}/hello-lambda/python"
    output_path = "${path.module}/hello-lambda/archive/hello-lambda.zip"
}

resource "aws_lambda_function" "hello_lambda_func" {
    filename            = "${path.module}/hello-lambda/archive/hello-lambda.zip"
    source_code_hash    = data.archive_file.hello_lambda_code.output_base64sha256
    function_name       = var.function_name
    role                = aws_iam_role.lambda_role.arn
    handler             = "${var.function_name}.lambda_handler"
    runtime             = "python3.8"
    depends_on          = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}

resource "aws_lambda_function" "hello_exception_lambda_func" {
    filename            = "${path.module}/hello-lambda/archive/hello-lambda.zip"
    source_code_hash    = data.archive_file.hello_lambda_code.output_base64sha256
    function_name       = var.function_exception_name
    role                = aws_iam_role.lambda_role.arn
    handler             = "${var.function_exception_name}.lambda_handler"
    runtime             = "python3.8"
    depends_on          = [aws_iam_role_policy_attachment.attach_iam_policy_to_iam_role]
}