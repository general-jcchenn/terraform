{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*",
                "sns:*",
                "states:*",
                "sqs:*",
                "lambda:InvokeFunction",
                "iam:ListRoles",
                "iam:ListInstanceProfiles",
                "iam:PassRole",
                "cloudwatch:*"
            ],
            "Resource": "*"
        }
    ]
}